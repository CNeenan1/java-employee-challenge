package com.example.rqchallenge;

import com.example.rqchallenge.employees.EmployeeRepository;
import com.example.rqchallenge.employees.EmployeeService;
import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.domain.Employee;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
public class EmployeeServiceTests {

    private static final int EMPLOYEE_ONE_ID = 1;
    private static final int EMPLOYEE_TWO_ID = 2;
    private static final int EMPLOYEE_THREE_ID = 3;
    private static final String EMPLOYEE_ONE_NAME = "James Bond";
    private static final String EMPLOYEE_TWO_NAME = "Donald Duck";
    private static final String EMPLOYEE_THREE_NAME = "Elon Musk";
    private static final int EMPLOYEE_ONE_AGE = 7;
    private static final int EMPLOYEE_TWO_AGE = 100;
    private static final int EMPLOYEE_THREE_AGE = 52;
    private static final int EMPLOYEE_ONE_SALARY = 7;
    private static final int EMPLOYEE_TWO_SALARY = 100;
    private static final int EMPLOYEE_THREE_SALARY = 1000000;
    private static final Employee EMPLOYEE_ONE = new Employee(EMPLOYEE_ONE_ID,
            EMPLOYEE_ONE_NAME, EMPLOYEE_ONE_SALARY, EMPLOYEE_ONE_AGE, null);
    private static final Employee EMPLOYEE_TWO = new Employee(EMPLOYEE_TWO_ID,
            EMPLOYEE_TWO_NAME, EMPLOYEE_TWO_SALARY, EMPLOYEE_TWO_AGE, null);
    private static final Employee EMPLOYEE_THREE = new Employee(EMPLOYEE_THREE_ID,
            EMPLOYEE_THREE_NAME, EMPLOYEE_THREE_SALARY, EMPLOYEE_THREE_AGE, null);
    private static final List<Employee> EMPLOYEES = new ArrayList<>();
    private static final String SEARCH_STRING = "elon";
    private static final CreateEmployee CREATE_EMPLOYEE = new CreateEmployee();
    private static final int GENERATED_EMPLOYEE_ID = 4;

    @BeforeAll
    public static void setup() {
        EMPLOYEES.add(EMPLOYEE_ONE);
        EMPLOYEES.add(EMPLOYEE_TWO);
        EMPLOYEES.add(EMPLOYEE_THREE);
    }

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    public void getAllEmployees_returnsEmployees() {
        when(employeeRepository.getAllEmployees()).thenReturn(EMPLOYEES);

        List<Employee> employees = employeeService.getAllEmployees();

        assertThat(employees, is(EMPLOYEES));
    }

    @Test
    public void getEmployeesByNameSearch_returnsEmployees() {
        when(employeeRepository.getAllEmployees()).thenReturn(EMPLOYEES);

        List<Employee> employees = employeeService.getEmployeesByNameSearch(SEARCH_STRING);

        assertThat(employees.size(), is(1));
        assertThat(employees.get(0), is(EMPLOYEE_THREE));
    }

    @Test
    public void getEmployeeById_returnsEmployee() {
        when(employeeService.getEmployeeById(EMPLOYEE_ONE_ID)).thenReturn(EMPLOYEE_ONE);

        Employee employee = employeeService.getEmployeeById(EMPLOYEE_ONE_ID);

        assertThat(employee, is(EMPLOYEE_ONE));
    }

    @Test
    public void getHighestSalaryOfEmployees_returnsHighestSalary() {
        when(employeeRepository.getAllEmployees()).thenReturn(EMPLOYEES);

        Integer highestSalary = employeeService.getHighestSalaryOfEmployees();

        assertThat(highestSalary, is(EMPLOYEE_THREE_SALARY));
    }

    @Test
    public void getTopLimitHighestEarningEmployeeNames() {
        when(employeeRepository.getAllEmployees()).thenReturn(EMPLOYEES);

        List<String> highestEarningEmployeeNames = employeeService.getTopLimitHighestEarningEmployeeNames(2);

        assertThat(highestEarningEmployeeNames.size(), is(2));
        assertTrue(highestEarningEmployeeNames.contains(EMPLOYEE_TWO_NAME));
        assertTrue(highestEarningEmployeeNames.contains(EMPLOYEE_THREE_NAME));
    }

    @Test
    public void createEmployee_generatesId() {
        when(employeeRepository.createEmployee(CREATE_EMPLOYEE)).thenReturn(GENERATED_EMPLOYEE_ID);

        int generatedEmployeeId = employeeRepository.createEmployee(CREATE_EMPLOYEE);

        assertThat(generatedEmployeeId, is(GENERATED_EMPLOYEE_ID));
    }

    @Test
    public void deleteEmployeeById_callsRepository() {
        employeeService.deleteEmployeeById(EMPLOYEE_ONE_ID);

        verify(employeeRepository, times(1)).deleteEmployeeById(EMPLOYEE_ONE_ID);
    }
}
