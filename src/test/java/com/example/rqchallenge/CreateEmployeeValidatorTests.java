package com.example.rqchallenge;

import com.example.rqchallenge.employees.CreateEmployeeValidator;
import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.exception.CreateEmployeeValidationException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CreateEmployeeValidatorTests {

    private static final String EMPLOYEE_NAME = "James Bond";
    private static final int VALID_AGE = 10;
    private static final int VALID_SALARY = 10;
    private static final int INVALID_AGE = -10;
    private static final int INVALID_SALARY = -10;

    @InjectMocks
    private CreateEmployeeValidator createEmployeeValidator;

    @Test
    public void validateCreateEmployee_success_noException() {
        CreateEmployee validCreateEmployee =
                new CreateEmployee(EMPLOYEE_NAME, VALID_AGE, VALID_SALARY);

        createEmployeeValidator.validateCreateEmployee(validCreateEmployee);
    }

    @Test
    public void validateCreateEmployee_negativeAge_throwsException() {
        CreateEmployee invalidCreateEmployee =
                new CreateEmployee(EMPLOYEE_NAME, INVALID_AGE, VALID_SALARY);

        assertThrows(CreateEmployeeValidationException.class, () ->
                createEmployeeValidator.validateCreateEmployee(invalidCreateEmployee));
    }

    @Test
    public void validateCreateEmployee_negativeSalary_throwsException() {
        CreateEmployee invalidCreateEmployee =
                new CreateEmployee(EMPLOYEE_NAME, VALID_AGE, INVALID_SALARY);

        assertThrows(CreateEmployeeValidationException.class, () ->
                createEmployeeValidator.validateCreateEmployee(invalidCreateEmployee));
    }
}
