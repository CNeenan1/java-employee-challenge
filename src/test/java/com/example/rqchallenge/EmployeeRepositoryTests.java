package com.example.rqchallenge;

import com.example.rqchallenge.employees.EmployeeRepository;
import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.domain.Employee;
import com.example.rqchallenge.employees.exception.EmployeeNotFoundException;
import com.example.rqchallenge.employees.external.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class EmployeeRepositoryTests {

    private static final String BASE_URL = "https://test.com/";
    private static final Employee EMPLOYEE = new Employee();
    private static final CreateEmployee CREATE_EMPLOYEE = new CreateEmployee();
    private static final List<Employee> EMPLOYEES = List.of(EMPLOYEE);
    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String EMPLOYEES_PATH = "employees";
    private static final String EMPLOYEE_PATH = "employee";
    private static final String CREATE_PATH = "create";
    private static final String DELETE_PATH = "delete";
    private static final int EMPLOYEE_ID = 1;
    private static final int GENERATED_EMPLOYEE_ID = 2;

    private final RestTemplate employeeRestTemplate = mock(RestTemplate.class);

    private final EmployeeRepository employeeRepository = new EmployeeRepository(employeeRestTemplate, BASE_URL);

    @Test
    public void getAllEmployees_success_returnsEmployees() {
        ResponseEntity<EmployeeListResponse> response = getAllEmployeesSuccessfulResponse();
        String getAllEmployeesUrl = BASE_URL + EMPLOYEES_PATH;
        when(employeeRestTemplate.getForEntity(getAllEmployeesUrl, EmployeeListResponse.class))
                .thenReturn(response);

        List<Employee> employeesResponse = employeeRepository.getAllEmployees();

        assertThat(employeesResponse, is(EMPLOYEES));
    }

    @Test
    public void getAllEmployees_failure_throwsException() {
        ResponseEntity<EmployeeListResponse> response = getAllEmployeesErrorResponse();
        String getAllEmployeesUrl = BASE_URL + EMPLOYEES_PATH;
        when(employeeRestTemplate.getForEntity(getAllEmployeesUrl, EmployeeListResponse.class))
                .thenReturn(response);

        assertThrows(RuntimeException.class, employeeRepository::getAllEmployees);
    }

    @Test
    public void getEmployeeById_success_returnsEmployee() {
        ResponseEntity<EmployeeResponse> response = getEmployeeByIdSuccessfulResponse();
        String getAllEmployeesUrl = BASE_URL + EMPLOYEE_PATH + "/" + EMPLOYEE_ID;
        when(employeeRestTemplate.getForEntity(getAllEmployeesUrl, EmployeeResponse.class))
                .thenReturn(response);

        Employee employee = employeeRepository.getEmployeeById(EMPLOYEE_ID);

        assertThat(employee, is(EMPLOYEE));
    }

    @Test
    public void getEmployeeById_notFound_throwsException() {
        ResponseEntity<EmployeeResponse> response = getEmployeeByIdNotFoundResponse();
        String getAllEmployeesUrl = BASE_URL + EMPLOYEE_PATH + "/" + EMPLOYEE_ID;
        when(employeeRestTemplate.getForEntity(getAllEmployeesUrl, EmployeeResponse.class))
                .thenReturn(response);

        assertThrows(EmployeeNotFoundException.class, () ->
                employeeRepository.getEmployeeById(EMPLOYEE_ID));
    }

    @Test
    public void getEmployeeById_failure_throwsException() {
        ResponseEntity<EmployeeResponse> response = getEmployeeByIdErrorResponse();
        String getAllEmployeesUrl = BASE_URL + EMPLOYEE_PATH + "/" + EMPLOYEE_ID;
        when(employeeRestTemplate.getForEntity(getAllEmployeesUrl, EmployeeResponse.class))
                .thenReturn(response);

        assertThrows(RuntimeException.class, () ->
                employeeRepository.getEmployeeById(EMPLOYEE_ID));
    }

    @Test
    public void createEmployee_success_returnsGeneratedId() {
        ResponseEntity<CreateEmployeeResponse> response = createEmployeeSuccessfulResponse();
        String createEmployeeUrl = BASE_URL + CREATE_PATH;
        when(employeeRestTemplate.postForEntity(
                createEmployeeUrl, CREATE_EMPLOYEE, CreateEmployeeResponse.class))
                .thenReturn(response);

        int generatedEmployeeId = employeeRepository.createEmployee(CREATE_EMPLOYEE);

        assertThat(generatedEmployeeId, is(GENERATED_EMPLOYEE_ID));
    }

    @Test
    public void createEmployee_failure_throwsException() {
        ResponseEntity<CreateEmployeeResponse> response = createEmployeeErrorResponse();
        String createEmployeeUrl = BASE_URL + CREATE_PATH;
        when(employeeRestTemplate.postForEntity(
                createEmployeeUrl, CREATE_EMPLOYEE, CreateEmployeeResponse.class))
                .thenReturn(response);

        assertThrows(RuntimeException.class, () ->
                employeeRepository.createEmployee(CREATE_EMPLOYEE));

    }

    @Test
    public void deleteEmployeeById_success() {
        ResponseEntity<DeleteEmployeeByIdResponse> response = deleteEmployeeByIdSuccessfulResponse();
        String deleteEmployeeByIdUrl = BASE_URL + DELETE_PATH + "/" + EMPLOYEE_ID;
        when(employeeRestTemplate.exchange(
                deleteEmployeeByIdUrl, HttpMethod.DELETE,
                null, DeleteEmployeeByIdResponse.class))
                .thenReturn(response);

        employeeRepository.deleteEmployeeById(EMPLOYEE_ID);
    }

    @Test
    public void deleteEmployeeById_failure_throwsException() {
        ResponseEntity<DeleteEmployeeByIdResponse> response = deleteEmployeeByIdErrorResponse();
        String deleteEmployeeByIdUrl = BASE_URL + DELETE_PATH + "/" + EMPLOYEE_ID;
        when(employeeRestTemplate.exchange(
                deleteEmployeeByIdUrl, HttpMethod.DELETE,
                null, DeleteEmployeeByIdResponse.class))
                .thenReturn(response);

        assertThrows(RuntimeException.class, () -> employeeRepository.deleteEmployeeById(EMPLOYEE_ID));
    }

    private ResponseEntity<EmployeeListResponse> getAllEmployeesSuccessfulResponse() {
        EmployeeListResponse responseBody = new EmployeeListResponse(SUCCESS, EMPLOYEES);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ResponseEntity<EmployeeListResponse> getAllEmployeesErrorResponse() {
        EmployeeListResponse responseBody = new EmployeeListResponse(FAILURE, emptyList());
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ResponseEntity<EmployeeResponse> getEmployeeByIdSuccessfulResponse() {
        EmployeeResponse responseBody = new EmployeeResponse(SUCCESS, EMPLOYEE);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ResponseEntity<EmployeeResponse> getEmployeeByIdNotFoundResponse() {
        EmployeeResponse responseBody = new EmployeeResponse(SUCCESS, null);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ResponseEntity<EmployeeResponse> getEmployeeByIdErrorResponse() {
        EmployeeResponse responseBody = new EmployeeResponse(FAILURE, null);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ResponseEntity<CreateEmployeeResponse> createEmployeeSuccessfulResponse() {
        CreateEmployeeResponseData createEmployeeResponseData = new CreateEmployeeResponseData();
        createEmployeeResponseData.setId(GENERATED_EMPLOYEE_ID);
        CreateEmployeeResponse responseBody = new CreateEmployeeResponse(SUCCESS, createEmployeeResponseData);
        return new ResponseEntity<>(responseBody, HttpStatus.CREATED);
    }

    private ResponseEntity<CreateEmployeeResponse> createEmployeeErrorResponse() {
        CreateEmployeeResponse responseBody = new CreateEmployeeResponse(FAILURE, null);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ResponseEntity<DeleteEmployeeByIdResponse> deleteEmployeeByIdSuccessfulResponse() {
        DeleteEmployeeByIdResponse responseBody = new DeleteEmployeeByIdResponse(SUCCESS, "");
        return new ResponseEntity<>(responseBody, HttpStatus.NO_CONTENT);
    }

    private ResponseEntity<DeleteEmployeeByIdResponse> deleteEmployeeByIdErrorResponse() {
        DeleteEmployeeByIdResponse responseBody = new DeleteEmployeeByIdResponse(FAILURE, "");
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }
}