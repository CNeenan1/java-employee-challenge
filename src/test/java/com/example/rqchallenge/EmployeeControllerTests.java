package com.example.rqchallenge;

import com.example.rqchallenge.employees.CreateEmployeeValidator;
import com.example.rqchallenge.employees.EmployeeController;
import com.example.rqchallenge.employees.EmployeeService;
import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.domain.Employee;
import com.example.rqchallenge.employees.exception.CreateEmployeeValidationException;
import com.example.rqchallenge.employees.exception.EmployeeNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
public class EmployeeControllerTests {

    private static final Employee EMPLOYEE = new Employee();
    private static final List<Employee> EMPLOYEES = List.of(EMPLOYEE);
    private static final String SEARCH_STRING = "James Bond";
    private static final int EMPLOYEE_ID = 5;
    private static final Integer HIGHEST_SALARY = 30000;
    private static final List<String> HIGHEST_EARNING_EMPLOYEES = List.of(SEARCH_STRING);
    private static final CreateEmployee CREATE_EMPLOYEE = new CreateEmployee();
    private static final int GENERATED_EMPLOYEE_ID = 6;
    private static final String VALIDATION_ERROR_BODY = "There was a validation error!";

    @Mock
    private EmployeeService employeeService;

    @Mock
    private CreateEmployeeValidator createEmployeeValidator;

    @InjectMocks
    private EmployeeController employeeController;

    @Test
    public void getAllEmployees_success_returns200() {
        when(employeeService.getAllEmployees()).thenReturn(EMPLOYEES);

        ResponseEntity<List<Employee>> response = employeeController.getAllEmployees();

        assertThat(response.getBody(), is(EMPLOYEES));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getAllEmployees_failure_returns500() {
        when(employeeService.getAllEmployees()).thenThrow(new RuntimeException());

        ResponseEntity<List<Employee>> response = employeeController.getAllEmployees();

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }


    @Test
    public void getEmployeesByNameSearch_success_returns200() {
        when(employeeService.getEmployeesByNameSearch(SEARCH_STRING)).thenReturn(EMPLOYEES);

        ResponseEntity<List<Employee>> response = employeeController.getEmployeesByNameSearch(SEARCH_STRING);

        assertThat(response.getBody(), is(EMPLOYEES));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getEmployeesByNameSearch_failure_returns500() {
        when(employeeService.getEmployeesByNameSearch(SEARCH_STRING)).thenThrow(new RuntimeException());

        ResponseEntity<List<Employee>> response = employeeController.getEmployeesByNameSearch(SEARCH_STRING);

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @Test
    public void getEmployeesById_success_returns200() {
        when(employeeService.getEmployeeById(EMPLOYEE_ID)).thenReturn(EMPLOYEE);

        ResponseEntity<Employee> response = employeeController.getEmployeeById(EMPLOYEE_ID);

        assertThat(response.getBody(), is(EMPLOYEE));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getEmployeesById_notFound_returns404() {
        when(employeeService.getEmployeeById(EMPLOYEE_ID)).thenThrow(new EmployeeNotFoundException());

        ResponseEntity<Employee> response = employeeController.getEmployeeById(EMPLOYEE_ID);

        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void getEmployeesById_failure_returns500() {
        when(employeeService.getEmployeeById(EMPLOYEE_ID)).thenThrow(new RuntimeException());

        ResponseEntity<Employee> response = employeeController.getEmployeeById(EMPLOYEE_ID);

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @Test
    public void getHighestSalaryOfEmployees_success_returns200() {
        when(employeeService.getHighestSalaryOfEmployees()).thenReturn(HIGHEST_SALARY);

        ResponseEntity<Integer> response = employeeController.getHighestSalaryOfEmployees();

        assertThat(response.getBody(), is(HIGHEST_SALARY));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getHighestSalaryOfEmployees_failure_returns500() {
        when(employeeService.getHighestSalaryOfEmployees()).thenThrow(new RuntimeException());

        ResponseEntity<Integer> response = employeeController.getHighestSalaryOfEmployees();

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @Test
    public void getTopTenHighestEarningEmployeeNames_success_returns200() {
        when(employeeService.getTopLimitHighestEarningEmployeeNames(10)).thenReturn(HIGHEST_EARNING_EMPLOYEES);

        ResponseEntity<List<String>> response = employeeController.getTopTenHighestEarningEmployeeNames();

        assertThat(response.getBody(), is(HIGHEST_EARNING_EMPLOYEES));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getTopTenHighestEarningEmployeeNames_failure_returns500() {
        when(employeeService.getTopLimitHighestEarningEmployeeNames(10)).thenThrow(new RuntimeException());

        ResponseEntity<List<String>> response = employeeController.getTopTenHighestEarningEmployeeNames();

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @Test
    public void createEmployee_success_returns201() {
        when(employeeService.createEmployee(CREATE_EMPLOYEE)).thenReturn(GENERATED_EMPLOYEE_ID);

        ResponseEntity<Object> response = employeeController.createEmployee(CREATE_EMPLOYEE);

        assertThat(response.getBody(), is(GENERATED_EMPLOYEE_ID));
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
    }

    @Test
    public void createEmployee_validationError_returns400() {
        when(employeeService.createEmployee(CREATE_EMPLOYEE))
                .thenThrow(new CreateEmployeeValidationException(VALIDATION_ERROR_BODY));

        ResponseEntity<Object> response = employeeController.createEmployee(CREATE_EMPLOYEE);

        assertThat(response.getBody(), is(VALIDATION_ERROR_BODY));
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void createEmployee_failure_returns500() {
        when(employeeService.createEmployee(CREATE_EMPLOYEE)).thenThrow(new RuntimeException());

        ResponseEntity<Object> response = employeeController.createEmployee(CREATE_EMPLOYEE);

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @Test
    public void deleteEmployeeById_success_returns204() {
        ResponseEntity<Void> response = employeeController.deleteEmployeeById(EMPLOYEE_ID);

        verify(employeeService, times(1)).deleteEmployeeById(EMPLOYEE_ID);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void deleteEmployeeById_success_returns500() {
        doThrow(new RuntimeException()).when(employeeService).deleteEmployeeById(EMPLOYEE_ID);

        ResponseEntity<Void> response = employeeController.deleteEmployeeById(EMPLOYEE_ID);

        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }
}
