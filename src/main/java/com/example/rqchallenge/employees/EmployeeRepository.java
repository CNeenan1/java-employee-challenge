package com.example.rqchallenge.employees;

import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.domain.Employee;
import com.example.rqchallenge.employees.exception.EmployeeNotFoundException;
import com.example.rqchallenge.employees.external.CreateEmployeeResponse;
import com.example.rqchallenge.employees.external.DeleteEmployeeByIdResponse;
import com.example.rqchallenge.employees.external.EmployeeListResponse;
import com.example.rqchallenge.employees.external.EmployeeResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Repository
public class EmployeeRepository {

    private static final String SUCCESS = "success";
    private static final String EMPLOYEE_PATH = "employee";
    private static final String EMPLOYEES_PATH = "employees";
    private static final String CREATE_PATH = "create";
    private static final String DELETE_PATH = "delete";

    private final RestTemplate employeeRestTemplate;
    private final String baseUrl;

    public EmployeeRepository(RestTemplate employeeRestTemplate,
                              @Value("${external.api.base.url}") String baseUrl) {
        this.employeeRestTemplate = employeeRestTemplate;
        this.baseUrl = baseUrl;
    }

    public List<Employee> getAllEmployees() {
        final String allEmployeesUrl = baseUrl + EMPLOYEES_PATH;
        ResponseEntity<EmployeeListResponse> employeeListResponseEntity = employeeRestTemplate
                .getForEntity(allEmployeesUrl, EmployeeListResponse.class);
        EmployeeListResponse employeeListResponse = employeeListResponseEntity.getBody();

        assert employeeListResponse != null;
        if (!employeeListResponse.getStatus().equals(SUCCESS)) {
            throw new RuntimeException("There was an error getting all employees in the external API");
        }

        return employeeListResponse.getData();
    }

    public Employee getEmployeeById(int id) {
        final String employeeByIdUrl = baseUrl + EMPLOYEE_PATH + "/" + id;
        ResponseEntity<EmployeeResponse> singleEmployeeResponseEntity = employeeRestTemplate
                .getForEntity(employeeByIdUrl, EmployeeResponse.class);
        EmployeeResponse employeeResponse = singleEmployeeResponseEntity.getBody();

        assert employeeResponse != null;
        if (!employeeResponse.getStatus().equals(SUCCESS)) {
            throw new RuntimeException("There was an error getting the employee in the external API");
        }

        Employee employee = employeeResponse.getData();

        if (employee == null) {
            throw new EmployeeNotFoundException();
        }

        return employee;
    }

    public int createEmployee(CreateEmployee createEmployee) {
        final String createEmployeeUrl = baseUrl + CREATE_PATH;
        ResponseEntity<CreateEmployeeResponse> createEmployeeResponseEntity = employeeRestTemplate
                .postForEntity(createEmployeeUrl, createEmployee, CreateEmployeeResponse.class);
        CreateEmployeeResponse createEmployeeResponse = createEmployeeResponseEntity.getBody();

        assert createEmployeeResponse != null;
        if (!createEmployeeResponse.getStatus().equals(SUCCESS)) {
            throw new RuntimeException("There was an error creating the employee in the external API");
        }

        return createEmployeeResponse.getData().getId();
    }

    public void deleteEmployeeById(int id) {
        final String deleteEmployeeByIdUrl = baseUrl + DELETE_PATH + "/" + id;
        ResponseEntity<DeleteEmployeeByIdResponse> deleteEmployeeResponseEntity = employeeRestTemplate
                .exchange(deleteEmployeeByIdUrl, HttpMethod.DELETE,
                        null, DeleteEmployeeByIdResponse.class);
        DeleteEmployeeByIdResponse deleteEmployeeByIdResponse = deleteEmployeeResponseEntity.getBody();

        assert  deleteEmployeeByIdResponse != null;
        if (! deleteEmployeeByIdResponse.getStatus().equals(SUCCESS)) {
            throw new RuntimeException("There was an error deleting the employee in the external API");
        }
    }
}
