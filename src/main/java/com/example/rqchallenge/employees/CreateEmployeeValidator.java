package com.example.rqchallenge.employees;

import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.exception.CreateEmployeeValidationException;
import org.springframework.stereotype.Component;

@Component
public class CreateEmployeeValidator {

    public void validateCreateEmployee(CreateEmployee createEmployee) {
        int age = createEmployee.getAge();
        boolean ageIsNegative = age < 0;
        if (ageIsNegative) {
            throw new CreateEmployeeValidationException("Validation error on value: "
                    + age + ". Employee age cannot be negative!");
        }

        int salary = createEmployee.getSalary();
        boolean salaryIsNegative = salary < 0;
        if (salaryIsNegative) {
            throw new CreateEmployeeValidationException("Validation error on value: "
                    + salary + ". Employee salary cannot be negative!");
        }
    }
}
