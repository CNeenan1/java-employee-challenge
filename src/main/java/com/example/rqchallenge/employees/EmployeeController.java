package com.example.rqchallenge.employees;

import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.domain.Employee;
import com.example.rqchallenge.employees.exception.CreateEmployeeValidationException;
import com.example.rqchallenge.employees.exception.EmployeeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * Note for reviewer - I have error logged to std out for development purposes.
 * In a production application, I would log to something like ELK stack.
 */

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final CreateEmployeeValidator createEmployeeValidator;

    @Autowired
    public EmployeeController(EmployeeService employeeService,
                              CreateEmployeeValidator createEmployeeValidator) {
        this.employeeService = employeeService;
        this.createEmployeeValidator = createEmployeeValidator;
    }

    /**
     * Endpoint to get all employees.
     * @return 200 OK with list of all employees if successful.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        try {
            List<Employee> employees = employeeService.getAllEmployees();
            return ResponseEntity.ok(employees);
        } catch (Exception ex) {
            System.out.println("There was an error getting all employees: " + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Endpoint to get all employees where their name contains a search value.
     * @param searchString the search value the user is searching for in the list of employees names.
     * @return 200 OK with list of employees which contain the name if successful.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @GetMapping("/employees/search/{searchString}")
    public ResponseEntity<List<Employee>> getEmployeesByNameSearch(@PathVariable String searchString) {
        try {
            List<Employee> employees = employeeService.getEmployeesByNameSearch(searchString);
            return ResponseEntity.ok(employees);
        } catch (Exception ex) {
            System.out.println("There was an error getting employees by name "
                    + searchString + ": " + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Endpoint to get a single employee record by their unique identifier.
     * @param id the unique identifier for the employee to get.
     * @return 200 OK with employee details for the unique identifier if successful.
     * 404 NOT FOUND if no employee exists with the unique identifier.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable int id) {
        try {
            Employee employee = employeeService.getEmployeeById(id);
            return ResponseEntity.ok(employee);
        } catch (EmployeeNotFoundException ex) {
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {
            System.out.println("There was an error getting employee by id "
                    + id + ": " + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Endpoint to get the highest salary of all employees.
     * @return 200 OK with the highest salary of all employees if successful.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @GetMapping("/employees/highestSalary")
    public ResponseEntity<Integer> getHighestSalaryOfEmployees() {
        try {
            Integer highestSalary = employeeService.getHighestSalaryOfEmployees();
            if (highestSalary == null) {
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(highestSalary);
        } catch (Exception ex) {
            System.out.println("There was an error getting the highest salary of employees: "
                    + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Endpoint to get the names of the top ten highest earning employees
     * @return 200 OK with the names of the ten highest earning employees if successful.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @GetMapping("/employees/topTenHighestEarningEmployeeNames")
    public ResponseEntity<List<String>> getTopTenHighestEarningEmployeeNames() {
        try {
            List<String> topTenHighestEarningEmployeeNames =
                    employeeService.getTopLimitHighestEarningEmployeeNames(10);
            return ResponseEntity.ok(topTenHighestEarningEmployeeNames);
        } catch (Exception ex) {
            System.out.println("There was an error getting the top 10 highest earning employees: "
                    + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Endpoint to create an employee record.
     * @param employeeInput the details of the employee to create.
     * @return 201 created with the generated unique identifier for the new record if successful.
     * 400 BAD REQUEST with details of the issue if any validation errors in the input.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @PostMapping("/employee")
    public ResponseEntity<Object> createEmployee(@RequestBody CreateEmployee employeeInput) {
        try {
            createEmployeeValidator.validateCreateEmployee(employeeInput);
            int generatedEmployeeId = employeeService.createEmployee(employeeInput);
            return ResponseEntity.status(HttpStatus.CREATED).body(generatedEmployeeId);
        } catch (CreateEmployeeValidationException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            System.out.println("There was an error creating employee with input "
                    + employeeInput + ": " + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Endpoint to delete an employee record by the unique identifier.
     * @param id the unique identifier of the record to delete.
     * @return 204 NO CONTENT if successful.
     * 500 INTERNAL SERVER ERROR if any unexpected issues.
     */
    @DeleteMapping("employee/{id}")
    public ResponseEntity<Void> deleteEmployeeById(@PathVariable int id) {
        try {
            employeeService.deleteEmployeeById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception ex) {
            System.out.println("There was an error deleting employee with id "
                    + id + ": " + ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }
}
