package com.example.rqchallenge.employees;

import com.example.rqchallenge.employees.domain.CreateEmployee;
import com.example.rqchallenge.employees.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.getAllEmployees();
    }

    public List<Employee> getEmployeesByNameSearch(String searchString) {
        List<Employee> allEmployees = getAllEmployees();
        return allEmployees
                .stream()
                .filter(employee ->
                employee.getName().toLowerCase(Locale.ROOT).contains(searchString.toLowerCase(Locale.ROOT)))
                .collect(Collectors.toList());
    }

    public Employee getEmployeeById(int id) {
        return employeeRepository.getEmployeeById(id);
    }

    public Integer getHighestSalaryOfEmployees() {
        List<Employee> employeesSortedBySalary = getAllEmployeesSortedBySalary();
        if (employeesSortedBySalary.isEmpty()) {
            return null;
        }
        return employeesSortedBySalary.get(0).getSalary();
    }

    public List<String> getTopLimitHighestEarningEmployeeNames(int limit) {
        List<Employee> employeesSortedBySalary = getAllEmployeesSortedBySalary();
        return employeesSortedBySalary
                .stream()
                .limit(limit)
                .map(Employee::getName)
                .collect(Collectors.toList());
    }

    public int createEmployee(CreateEmployee employee) {
        return employeeRepository.createEmployee(employee);
    }

    public void deleteEmployeeById(int id) {
        employeeRepository.deleteEmployeeById(id);
    }

    private List<Employee> getAllEmployeesSortedBySalary() {
        List<Employee> employees = getAllEmployees();
        employees.sort(Comparator.comparing(Employee::getSalary).reversed());
        return employees;
    }
}
