package com.example.rqchallenge.employees.exception;

public class CreateEmployeeValidationException extends RuntimeException {
    public CreateEmployeeValidationException(String message) {
        super(message);
    }
}
