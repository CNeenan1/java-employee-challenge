package com.example.rqchallenge.employees.external;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateEmployeeResponse {
    private String status;
    private CreateEmployeeResponseData data;

    public CreateEmployeeResponse() {
    }

    public CreateEmployeeResponse(String status, CreateEmployeeResponseData data) {
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CreateEmployeeResponseData getData() {
        return data;
    }

    public void setData(CreateEmployeeResponseData data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateEmployeeResponse that = (CreateEmployeeResponse) o;
        return Objects.equals(status, that.status) && Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, data);
    }

    @Override
    public String toString() {
        return "CreateEmployeeResponse{" +
                "status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
